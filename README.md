# credit-card-cost 用卡费用统计工具

#### 介绍
为什么会有这个项目呢，一个朋友因为使用信用卡，经常要套现还另外一张信用卡，然后他需要统一每一次刷卡花费的费用，所以就用了这个项目，帮助他方便的统计套现费用。


#### 软件架构
使用uni-app开发的，面前只适配了小程序，其他的没有适配。

#### 体验

![小程序体验](https://images.gitee.com/uploads/images/2019/1118/113358_ae8d224b_554062.jpeg "gh_ea3c6bcbdfb1_258.jpg")

#### 安装教程

1.  使用命令 git clone https://gitee.com/zcc.com/credit-card-cost.git,将项目克隆到本地
2.  推荐使用HBuilder打开项目
3.  运行到小程序开发工具


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
