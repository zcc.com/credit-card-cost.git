import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
	state:{
		apiBaseUrl: `https://127.0.0.1/api`,
		baseUrls:`https://127.0.0.1`,
		tokenName: `user`,
		miniAppId:``,
	}
})