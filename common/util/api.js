import request from '@/common/util/request.js'
import store from '@/common/store'

export default {
	login(param){
		param.wxapp_id = store.state.miniAppId;
		return request.post('/loginJson',param);
	},
	/**
	 * @param {Object} param
	 * 保存费率信息
	 */
	saveCost(param){
		return request.post('/json/cost/add',param);
	},
	/**
	 * @param {Object} param
	 * 修改费率信息
	 */
	updateCost(param){
		return request.post('/json/cost/edit',param);
	},
	/**
	 * @param {Object} param
	 * 删除费率信息
	 */
	deleteCost(param){
		return request.del(`/json/cost/remove/${param.id}`,param);
	},
	/**
	 * @param {Object} param
	 * 获取费率列表
	 */
	getCostList(param){
		return request.get('/json/cost/list',param);
	},
	/**
	 * @param {Object} param
	 * 保存交易信息
	 */
	saveTrade(param){
		return request.post('/json/trade/add',param);
	},
	/**
	 * @param {Object} param
	 * 修改交易信息
	 */
	updateTrade(param){
		return request.post('/json/trade/edit',param);
	},
	/**
	 * @param {Object} param
	 * 删除交易信息
	 */
	deleteTrade(param){
		return request.del(`/json/trade/remove/${param.id}`,param);
	},
	/**
	 * @param {Object} param
	 * 按年统计费用
	 */
	getTradeYearList(param){
		return request.get('/json/trade/getTradeYearList',param);
	},
	/**
	 * @param {Object} param
	 * 按年\月统计费用
	 */
	getTradeMonthList(param){
		return request.get('/json/trade/getTradeMonthList',param);
	},
	/**
	 * @param {Object} param
	 * 按年日统计费用
	 */
	getTradeDayList(param){
		return request.get('/json/trade/getTradeDayList',param);
	},
	/**
	 * @param {Object} param
	 * 获取日详情
	 */
	getTradeDayDetailList(param){
		return request.get('/json/trade/getTradeDayDetailList',param);
	},
	/**
	 * @param {Object} param
	 * 获取交易列表
	 */
	getTradeList(param){
		return request.get('/json/trade/list',param);
	}
}