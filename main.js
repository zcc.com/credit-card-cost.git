import Vue from 'vue'
import App from './App'
import api from '@/common/util/api.js'

const msg = (title,duration=1500,mask=false,icon="none") => {
	if(Boolean(title) === false){
		return;
	}
	uni.showToast({
		title:title,
		duration:duration,
		mask:mask,
		icon:icon
	})
}

Vue.config.productionTip = false
Vue.prototype.$api = {msg};
Vue.prototype.$net = api;

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
